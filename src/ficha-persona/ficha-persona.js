import { LitElement, html, css } from 'lit-element';
import {OrigenPersona} from '../origen-persona/origen-persona.js';
class FichaPersona  extends LitElement {

  static get styles() {
    return css`
      div {
          border: 1px solid;
          border-radius: 10px;
          padding: 10px;
          margin: 10px;
      }
    `;
  }

  static get properties() {
    return {
        nombre: {type:String},
        apellidos: {type:String},
        anyosAntiguedad: {type:Number},
        nivel: {type:String},
        foto: {type:Object},
        bg: {type:String}
        
    };
  }

  constructor() {
    super();
    this.nombre = "Pedro";
    this.apellido = "López López";
    this.anyosAntiguedad = 4;
    this.foto = {src: "src/ficha-persona/img/personas.jpg",
                 alt: "Foto persona"};
    this.bg = "aliceblue";
    
  }

  render() {
    return html`
      <div style="width:400px; background-color:${this.bg}">
        <label for="inombre">Nombre</label>
        <input type="text" id="inombre" name="inombre" value="${this.nombre}" @input="${this.updateNombre}"></input>
        <br/>
        <label for="iapellidos">Apellidos</label>
        <input type="text" id="iapellidos" name="iapellidos" value="${this.apellido}"></input>
        <br/>
        <label for="iantiguedad">Antiguedad</label>
        <input type="number" id="iantiguedad" name="iantiguedad" value="${this.anyosAntiguedad}" @input="${this.updateAntiguedad}"></input>
        <br/>
        <label for="inivel">Nivel</label>
        <input type="text" id="inivel" name="inivel" value="${this.nivel}" disable ></input>
        <br/>
        <origen-persona @origen-set="${this.origenChange}"></origen-persona>
        <img src="${this.foto.src}" height="200" width="200" alt="${this.foto.alt}" />
      </div>
    `;
  }
  updated(changedProperties){
    if(changedProperties.has("nombre")){
        console.log("Propiedad nombre cambiada. Valor anterior: "+ 
                    changedProperties.get("nombre") + " " + this.nombre);
    }

    if(changedProperties.has("anyosAntiguedad")){
        this.actualizarNivel();
    }
  }

  updateNombre(e){
      this.nombre = e.target.value;
  }

  updateAntiguedad(e){
      this.anyosAntiguedad = e.target.value;

  }

  actualizarNivel(){
      if(this.anyosAntiguedad >= 7){
          this.nivel = "Líder";
      }else if (this.anyosAntiguedad >= 5){
          this.nivel = "Senior";
      }else if (this.anyosAntiguedad >= 3){
          this.nivel = "Team";
      }else {
          this.nivel = "Junior";
      }
  }

  origenChange(e){
    this.origen = e.detail.message;

    if(this.origen === "USA"){
        this.bg = "pink";
    }else if (this.origen === "México"){
        this.bg = "lightgreen";
    }else if (this.origen === "Canadá"){
        this.bg = "lightyellow";
    }
  }

}

customElements.define('ficha-persona', FichaPersona);